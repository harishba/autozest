package com.amdocs.autozest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class BasicMethods {

	public static String socialName = "";
	public static String socialLink = "";
	public static List<WebElement> social;
	public static ExtentReports extent;
	public static ExtentTest test;
	
	public static void getFooterLinks() {
		WebElement links = AutoZestFlowTwo.oDriver
				.findElement(By.xpath("//ul[@class='app_footer_socialitems text_center pull_right']"));

		social = links.findElements(By.tagName("a"));

		System.out.println("The number of footer links are :" + social.size());

		System.out.println(
				"********************************************************************Footer Social Links**************************************************************************");

		for (int i = 0; i < social.size(); i++) {

			socialName = social.get(i).getAttribute("title");
			socialLink = social.get(i).getAttribute("href");

			System.out.println(socialName + "-" + socialLink);

			CreateAndWriteExcel.sheet.createRow(i);
			CreateAndWriteExcel.sheet.getRow(i).createCell(0).setCellValue(socialName);
			CreateAndWriteExcel.sheet.getRow(i).createCell(1).setCellValue(socialLink);

		}

		System.out.println(
				"*****************************************************************************************************************************************************************");

	}

	public static void getLinksOfLearnMore() throws InterruptedException {

		WebElement element1 = AutoZestFlowTwo.oDriver.findElement(By.cssSelector("div[ng-click *= 'Microservices']"));

		((JavascriptExecutor) AutoZestFlowTwo.oDriver).executeScript("arguments[0].click();", element1);

		String url1 = AutoZestFlowTwo.oDriver.getCurrentUrl();

		System.out.println(
				"********************************************************************Learn More Links**************************************************************************");

		System.out.println(url1);

		AutoZestFlowTwo.oDriver.navigate().back();
		AutoZestFlowTwo.oDriver.navigate().refresh();
		Thread.sleep(3000);

		WebElement element2 = AutoZestFlowTwo.oDriver.findElement(By.cssSelector("div[ng-click *= 'Cloud']"));

		((JavascriptExecutor) AutoZestFlowTwo.oDriver).executeScript("arguments[0].click();", element2);

		String url2 = AutoZestFlowTwo.oDriver.getCurrentUrl();

		System.out.println(url2);

		AutoZestFlowTwo.oDriver.navigate().back();
		AutoZestFlowTwo.oDriver.navigate().refresh();
		Thread.sleep(3000);

		WebElement element3 = AutoZestFlowTwo.oDriver.findElement(By.cssSelector("div[ng-click *= 'Cloud']"));

		((JavascriptExecutor) AutoZestFlowTwo.oDriver).executeScript("arguments[0].click();", element3);

		String url3 = AutoZestFlowTwo.oDriver.getCurrentUrl();

		System.out.println(url3);

		System.out.println(
				"*******************************************************************************************************************************************************************");

		AutoZestFlowTwo.oDriver.navigate().back();
		AutoZestFlowTwo.oDriver.navigate().refresh();
		Thread.sleep(3000);

	}

	public static void switchTabs() throws InterruptedException {

		ArrayList<String> tabs = new ArrayList<String>(AutoZestFlowTwo.oDriver.getWindowHandles());

		AutoZestFlowTwo.oDriver.switchTo().window(tabs.get(1));

		AutoZestFlowTwo.oDriver.findElement(By.xpath("//img[@class='lpimg']")).click();

		Thread.sleep(5000);

		AutoZestFlowTwo.oDriver.close();

		AutoZestFlowTwo.oDriver.switchTo().window(tabs.get(0));
	}

	public static void introText() throws InterruptedException {

		AutoZestFlowTwo.introText = AutoZestFlowTwo.oDriver.findElement(By.cssSelector(".about-col.single-column"))
				.getText();

		System.out.println(
				"********************************************************************Introduction Text**************************************************************************");

		System.out.println(AutoZestFlowTwo.introText);

		System.out.println(
				"****************************************************************************************************************************************************************");

	}

	public static void breadcrumbText() throws InterruptedException {

		AutoZestFlowTwo.breadcrumbText = AutoZestFlowTwo.oDriver.findElement(By.xpath("//ol[@class='breadcrumb']"))
				.getText();

		System.out.println(
				"********************************************************************Breadcrumb Text**************************************************************************");

		System.out.println("Breadcrumb Text are :-" + AutoZestFlowTwo.breadcrumbText);

		System.out.println(
				"****************************************************************************************************************************************************************");

	}
	
	public String getScreenShotPath(String testCaseName,WebDriver driver) throws IOException
	{
		TakesScreenshot ts=(TakesScreenshot) driver;
		File source =ts.getScreenshotAs(OutputType.FILE);
		String destinationFile = System.getProperty("user.dir")+"\\reports\\"+testCaseName+".png";
		FileUtils.copyFile(source,new File(destinationFile));
		return destinationFile;


	}
	
	
	
}
	