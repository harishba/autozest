package com.amdocs.autozest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutoZestFlowTwo {
	public static WebDriver oDriver;
	public static String breadcrumbText = "";
	public static String introText = "";
	
	@BeforeTest()
	public void driverInitialize() throws Exception
	{	
		
		ExtentReport a =new ExtentReport();
		
		a.getReportObject();
		
		
		WebDriverManager.chromedriver().setup();

		oDriver = new ChromeDriver();
		
	}
	
	
	@Parameters({ "URL"})
	@Test(priority = 1)
	public void testCaseOne(String URL) {

			oDriver.navigate().to(URL);

			oDriver.manage().window().maximize();	
		
	}

	@Parameters({"AcceptCookies"})
	@Test(priority = 2)
	public void testCaseTwo(String AcceptCookies) {	
	
		oDriver.findElement(By.cssSelector(AcceptCookies)).click();
		
	}

	@Test(priority = 3)
	public void testCaseThree() {	
		
		BasicMethods.getFooterLinks();	
		
	}
	@Parameters({"HumbergerMenu"})
	@Test(priority = 4)
	public void testCaseFour(String HumbergerMenu) {	
		
		oDriver.findElement(By.cssSelector(HumbergerMenu)).click();
	}
	
	
	
	
	@Parameters({"PracticeDriven"})
	@Test(priority = 5)
	public void testCaseFive(String PracticeDriven) {	
		
		oDriver.findElement(By.cssSelector(PracticeDriven)).click();
	}	
	
	@Test(priority = 6)
	public void testCaseSix() throws Exception {	
		
		BasicMethods.getLinksOfLearnMore();	
		
	}
	@Parameters({"Microservices"})
	@Test(priority = 7)
	public void testCaseSeven(String Microservices) {	
		
		WebElement element = oDriver.findElement(By.cssSelector(Microservices));

		((JavascriptExecutor) oDriver).executeScript("arguments[0].click();", element);
		
	}
	
	@Test(priority = 8)
	public void testCaseEight() throws Exception {	
		
		BasicMethods.breadcrumbText();	
		
	}
	
	@Parameters({ "Research"})
	@Test(priority = 9)
	public void testCaseNine(String Research) {	
		
		WebElement research = oDriver.findElement(By.cssSelector(Research));

		((JavascriptExecutor) oDriver).executeScript("arguments[0].click();", research);
		
	}
	
	@Test(priority = 10)
	public void testCaseTen() throws Exception {	
		
		BasicMethods.switchTabs();
		
	}
	
	@Test(priority = 11)
	public void testCaseEleven() throws Exception {	
		
		BasicMethods.introText();
		
		CreateAndWriteExcel.writeToExcel();
		
	}

	@Test(priority = 12)
	public void testFailOnPurpose() {	
		
		oDriver.findElement(By.cssSelector("randomly")).click();
	}	
			
	
	@AfterTest()
	public void driverQuit()
	{
	oDriver.quit();
	
	}
}
