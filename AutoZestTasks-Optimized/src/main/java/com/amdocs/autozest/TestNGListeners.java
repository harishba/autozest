package com.amdocs.autozest;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class TestNGListeners extends BasicMethods implements ITestListener {

	ExtentTest test;
	
	ExtentReport rep=new ExtentReport();
	
	ExtentReports extent=rep.getReportObject();	
	
	public void onTestStart(ITestResult result) {

		
		test=extent.createTest(result.getMethod().getMethodName());
		
	}

	public void onTestSuccess(ITestResult result) {

		test.log(Status.PASS,"Test Passed");
		
	}

	public void onTestFailure(ITestResult result) {

		test.fail(result.getThrowable());
	
		WebDriver driver =null;
		String testMethodName =result.getMethod().getMethodName();
		
		try {
			driver =(WebDriver)result.getTestClass().getRealClass().getDeclaredField("oDriver").get(result.getInstance());
		} catch(Exception e)
		{
			
		}
		try {
			getScreenShotPath(testMethodName,driver);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	public void onFinish(ITestContext context)
	{
		extent.flush();
	}
	
}
