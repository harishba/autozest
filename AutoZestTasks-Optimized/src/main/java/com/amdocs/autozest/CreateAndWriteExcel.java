package com.amdocs.autozest;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateAndWriteExcel {

	public static XSSFWorkbook workbook = new XSSFWorkbook();

	public static XSSFSheet sheet = workbook.createSheet("Result");

	public static void writeToExcel() throws Exception {

		int lastrownum = sheet.getLastRowNum();

		lastrownum += 2;

		sheet.createRow(lastrownum);

		sheet.getRow(lastrownum).createCell(0).setCellValue("Breadcrumb Text");
		sheet.autoSizeColumn(0);
		sheet.getRow(lastrownum).createCell(1).setCellValue(AutoZestFlowTwo.breadcrumbText);
		sheet.autoSizeColumn(1);

		lastrownum += 2;

		sheet.createRow(lastrownum);
		sheet.getRow(lastrownum).createCell(0).setCellValue("Introduction Text");
		sheet.getRow(lastrownum).createCell(1).setCellValue(AutoZestFlowTwo.introText);

		String fileName = new SimpleDateFormat("yyyyMMdd HHmmss").format(new Date());

		String current_user = System.getProperty("user.name");

		File file = new File("C:\\Users\\" + current_user + "\\Downloads\\Data-" + fileName + ".xlsx");

		System.out.println("Hi " + current_user + " excel file has been downloaded in Downloads directory. ");

		FileOutputStream fos = new FileOutputStream(file);
		workbook.write(fos);
		workbook.close();

	}
}
